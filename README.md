# GiphyAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.3.

## Instalacion 

1.- Correr el siguiente comando  npm install, esto permitira instalar todas las dependencias del proyect

2.- Una ves terminado la instalacion correr el comando ng serve -o, la cual permitira iniciar y abrir la ventana del nabegador o ng serve solo para inciar el server

En caso de solo iniciar el server, en el navegador de preferencia se puede accesar al desarrollo con la siguiente url http://localhost:4200/

## Tecnologias

1.-Angular

2.-Bootstrap 4

3.-Font Awesome

4.-Scss o Sass