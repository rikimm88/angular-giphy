import { Component } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { GiphyService } from "./services/giphy.service"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
	img:string[]=[];
	gif = {
		query:""
	}
	constructor(private http: GiphyService){
		this.get();
	}

	get(){
		this.http.getGiphy().subscribe(data=>{
			console.log(data)
			this.img = data.data;
		})
	}
	search(){
		// console.log()
		this.http.serchGiphy(this.gif.query).subscribe(data=>{
			 console.log(data.data)
			this.img = data.data;
		})
	}
}
